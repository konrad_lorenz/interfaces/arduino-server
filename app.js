const http = require("http");
const PORT = process.env.PORT || 3001;


const { Board, Led, Servo, Sensor } = require("johnny-five");
const board = new Board();

board.on("ready", () => {
    const led = new Led(13);

    // Valores de calibración
    const analogMin = 0; // Valor analógico cuando el objeto está muy cerca
    const analogMax = 1023; // Valor analógico cuando el objeto está muy lejos
    const distanceMin = 5; // Distancia en cm cuando el objeto está muy cerca
    const distanceMax = 30; // Distancia en cm cuando el objeto está muy lejos


    board.repl.inject({
        led
    });

    const server = http.createServer(async (req, res) => {

        if (req.url === "/prender" && req.method === "GET") {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.write("Hi there, This is a Vanilla Node.js API");
            res.end();
            led.on();

        }

        else if (req.url === "/apagar" && req.method === "GET") {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.write("Hi there, This is a Vanilla Node.js API 2");
            res.end();
            led.off();
        }
        else if (req.url === "/left" && req.method === "GET") {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.write("estoy en el motor");
            const servo = new Servo({
                pin: 13,
                startAt: 10,
            });

            servo.to(180);
            board.wait(1000, function() {
                // servo.to(0);
            });
        }
        else if (req.url === "/right" && req.method === "GET") {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.write("estoy en el motor");
            const servo = new Servo({
                pin: 13,
                startAt: 10,
            });
            // servo.to(180);
            // board.wait(1000, function() {
            //     // servo.to(0);
            // });
        }
        else if (req.url === "/prox" && req.method === "GET") {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.write("estoy en el sensor de proximidad");

            const sensor = new Sensor("A0");

            // sensor.on("change", function() {
            //     console.log("Distancia: " + this.value);
            // });
            sensor.on("data", function() {
                const analogValue = this.value;
                const distance = map(analogValue, analogMin, analogMax, distanceMin, distanceMax);
                console.log("Distancia: " + distance + " cm");
            });

        }
        else {
            res.writeHead(404, { "Content-Type": "application/json" });
            res.end(JSON.stringify({ message: "Route not found" }));
        }
    });

    server.listen(PORT, () => {
        console.log('server started on port: ${PORT}');
    });
});



function map(value, x1, x2, y1, y2) {
    return (value - x1) * (y2 - y1) / (x2 - x1) + y1;
}
